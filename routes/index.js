var express = require('express');
var session = require('express-session');
var mongodb = require('mongodb');
var url = require('url');
var querystring = require('querystring');
var multer = require('multer');



var router = express.Router();
/*MONGOOSE Shema*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var dbURL = 'mongodb://localhost/employee_directory';
var db = mongoose.connect(dbURL);
var Employee = require('../models/Employee');
var Department = require('../models/Department');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});


/* POST Login */
router.post('/Login', function (req, res) {
  var backURL = req.header('Referer') || '/';
  if (req.body.email && req.body.password) {
    var email = req.body.email;
    var password = req.body.password;
    if (email.trim() != '' && password.trim() != '' && email.length < 100 && password.length < 100) {
      Employee.findOne({
        "email": email,
        "password": password
      }, function (err, data) {
        if (err) {
          res.send("Logged in failed: " + err);
        } else if (data) {
          req.session.email = data.email;
          req.session.name = data.name;
          res.redirect(backURL);
        } else {
          req.session.message = 'Logged in failed!';
          res.redirect(backURL);
        }
      });
    } else {
      req.session.message = 'Invalid email or password!';
      res.redirect(backURL);
    }
  } else {
    req.session.message = 'Lack of arguments!';
    res.redirect(backURL);
  }

});

/* GET Logout */
router.get('/Logout', function (req, res) {
  req.session.destroy();
  res.redirect('/');
});



/* View Employee List Page*/
router.get("/ViewEmployeeList", function (req, res) {
  var page = req.query.page ? req.query.page : 1; //default page = 1
  var items_per_page = 5;
  var start_point = (page - 1) * items_per_page;
  var search_query = req.query.search_query;
  var find_option = search_query ? {} : {};
  Employee.find({}) // All
          .skip(start_point) // Start geting items from the given page
          .populate('department') // Connect to Department
          .limit(items_per_page) // Limit number of record in result
          .exec(function (err, employees) {
            Employee.find().count(function (err, total_records) {
              res.render('ViewEmployeeList', {
                employee_list: employees,
                page: page,
                items_per_page: items_per_page,
                start_point: start_point,
                total_records: total_records
              });
            });
            //res.render('ViewEmployeeList', {
            //  employee_list: employees
            //});
          });
});
/* View Employee Page*/
router.get("/ViewEmployee", function (req, res) {
  var params = querystring.parse(url.parse(req.url).query);
  Employee.findOne({"email": params['email']}).populate('department')
          .exec(function (err, employee) {
            res.render('ViewEmployee', {employee: employee});
          });
});
//==============================================================================
function validateEmployee(email, name, address, job_title) {
  var MAX_LENGTH = 100;
  return (email && email.length < MAX_LENGTH && email.trim() != ''
          && name && name.length < MAX_LENGTH && name.trim() != ''
          && address && address.length < MAX_LENGTH && address.trim() != ''
          && job_title && job_title.length < MAX_LENGTH && job_title.trim() != ''
          );
}

/* Edit Employee Page*/
router.get("/EditEmployee", function (req, res) {
  console.log('Edit employee: Load form');
  Employee.findOne({"email": req.query.email})
          .populate('department')
          .exec(function (err, employee) {
            Department.find({}, function (err2, department_list) {
              res.render('EditEmployee', {
                employee: employee,
                department_list: department_list
              });
            });
          });
});
router.post("/EditEmployee", multer({dest: './public/images/avatars/'}, {limits: {fileSize: 2000000}}).single('avatar_url'), function (req, res) { // 
  console.log('Edit employee: POST FORM');
  // Check login first
  var backURL = req.header('Referer') || '/';
  if (!req.session.email) {
    res.send('You have not logged in, please log in first!<hr><a href="' + backURL + '">Go back the previous page</a>');
    return;
  } // -- end check login
  if (req.body.do_edit) {
    if (validateEmployee(req.body.email, req.body.name, req.body.address, req.body.job_title)) {
      var update_data = req.file ? {
        "name": req.body.name.trim(),
        "address": req.body.address.trim(),
        "department": req.body.department_id,
        "job_title": req.body.job_title.trim(),
        "avatar_url": 'images/avatars/' + req.file.filename
      } : {
        "name": req.body.name.trim(),
        "address": req.body.address.trim(),
        "department": req.body.department_id,
        "job_title": req.body.job_title.trim()
      };
      Employee.update({'email': req.body.email}, update_data, {multi: false}, function (err, result) {
        if (err) {
          req.session.message = 'Error while updating data:' + err;
        } else {
          req.session.message = 'Data updated';
          res.redirect('ViewEmployee?email=' + req.body.email);
        }
      });
    } else {
      console.log('Edit employee: Back');
      req.session.message = 'Employee data were not validated, please checck again!';
      req.session.back_data = {
        "name": req.body.name,
        "address": req.body.address,
        "department": req.body.department_id,
        "job_title": req.body.job_title
      };
      res.redirect(backURL);
    }

  } else {
    Employee.findOne({"email": req.body.email})
            .populate('department')
            .exec(function (err, employee) {
              Department.find({}, function (err2, department_list) {
                res.render('EditEmployee', {
                  employee: employee,
                  department_list: department_list
                });
              });
            });
  }
});
/* Delete Employee */
router.get("/DeleteEmployee", function (req, res) {
  // Check login first
  var backURL = req.header('Referer') || '/';
  if (!req.session.email) {
    res.send('You have not logged in, please log in first!<hr><a href="' + backURL + '">Go back the previous page</a>');
    return;
  } // -- end check login
  if (req.query.email) {
    Employee.find({'email': req.query.email}).remove(function (err, doc) {
      if (err) {
        res.send("Error while removing document from database: " + err);
      } else {
        req.session.message = 'Employee named "' + req.query.email + '" was successful removed';
        res.redirect('ViewEmployeeList');
      }
    });
  } else {
    var backURL = req.header('Referer') || '/';
    req.session.message = 'No email chosen to delete';
    res.redirect(backURL);
  }
});


/* GET Register page: Load register form*/
router.get('/Register', function (req, res) {
  // Check login first
  var backURL = req.header('Referer') || '/';
  if (req.session.email) {
    res.send('<big>You have logged in to the system as <b>' + req.session.email + '</b>, please <a href="Logout">Log out</a> first!<hr><a href="' + backURL + '">Go back the previous page</a></p>');
    return;
  } // -- end check login

  Department.find({}, function (err2, department_list) {
    res.render('Register', {department_list: department_list});
  });
});

/* POST Register page: Do register */
router.post('/Register', function (req, res) {
  var MAX_LENGTH = 100;
  //var backURL = req.header('Referer') || '/';
  if (req.body.do_register) {
    if (validateEmployee(req.body.email, req.body.name, req.body.address, req.body.job_title)) {
      if (req.body.password && req.body.password_confirmed && req.body.password == req.body.password_confirmed) {
        var new_employee = new Employee({
          "_id": new mongodb.ObjectID(),
          "email": req.body.email.trim(),
          "name": req.body.name.trim(),
          "password": req.body.password, // no trim password
          "address": req.body.address.trim(),
          "department": req.body.department_id,
          "job_title": req.body.job_title.trim(),
          "avatar_url": "images/avatars/default.png"
        });
        new_employee.save(function (err, data) {
          if (err) {
            res.send("Operation failed when inserting new employee to Database: " + err);
          } else {
            req.session.email = req.body.email;
            req.session.name = req.body.name;
            res.redirect('ViewEmployee?email=' + req.body.email);
          }
        });
      } else {
        req.session.message = 'Password was not confirmed!';
        req.session.back_data = {// Send failed data back to register form
          "email": req.body.email,
          "name": req.body.name,
          "password": req.body.password,
          "address": req.body.address,
          "department": req.body.department_id,
          "job_title": req.body.job_title
        };
        res.redirect('/Register');
      }
    } else {
      req.session.message = 'Data were not validated, register failed';
      req.session.back_data = {// Send failed data back to register form
        "email": req.body.email,
        "name": req.body.name,
        "password": req.body.password,
        "address": req.body.address,
        "department": req.body.department_id,
        "job_title": req.body.job_title
      };
      res.redirect('/Register');
    }
  } else {
    req.session.message = 'Not ready';
    res.redirect('/Register');
  }
});
/* View Department List Page*/
router.get("/ViewDepartmentList", function (req, res) {
  Department.find({})
          .populate('manager')
          .exec(function (err, department_list) {
            res.render('ViewDepartmentList', {
              department_list: department_list
            });
          });
});

/* View Department Page*/
router.get("/ViewDepartment", function (req, res) {
  Department.findOne({'_id': req.query.department_id})
          .populate('manager')
          .exec(function (err, department) {
            Employee.find({'department': department._id}).exec(function (err, employee_list) {
              res.render('ViewDepartment', {
                department: department,
                employee_list: employee_list
              });
            });
          });
});


//==============================================================================
function validateDepartment(department_id, department_name, office_number, manager_id) {
  var MAX_LENGTH = 100;
  return (department_id && department_name
          && department_name.length < MAX_LENGTH
          && department_name.trim() != ''
          && office_number
          && office_number.length < MAX_LENGTH
          && office_number.trim() != ''
          && manager_id);
}
/* Add Department Page*/
router.get("/AddNewDepartment", function (req, res) {
  // Check login first
  var backURL = req.header('Referer') || '/';
  if (!req.session.email) {
    res.send('You have not logged in, please log in first!<hr><a href="' + backURL + '">Go back the previous page</a>');
    return;
  } // -- end check login
  if (req.query.do_add) {
    var new_id = new mongodb.ObjectID();
    if (validateDepartment(
            new_id,
            req.query.department_name,
            req.query.office_number,
            req.query.manager_id)) {
      var new_department = new Department({
        "_id": new_id,
        "department_name": req.query.department_name.trim(),
        "office_number": req.query.office_number.trim(),
        "manager": req.query.manager_id
      });
      // Save new department
      new_department.save(function (err, data) {
        if (err) {
          res.send("Operation failed while inserting new department to Database: " + err);
        } else {
          // Move the selected manager to this department
          Employee.update({'_id': req.query.manager_id}, {
            "department": new_id,
            "job_title": 'Manager'
          }, {multi: false}, function (err, result) {
            res.redirect('ViewDepartment?department_id=' + new_id);
          });
        }
      });
    } else { // validation failed
      req.session.message = 'Department data were not validated, Please check again.';
      req.session.back_data = {// Send failed data back to register form
        "department_name": req.query.department_name,
        "office_number": req.query.office_number,
        "manager": req.query.manager_id
      };
      res.redirect(backURL);
    }
  } else {
    Employee.find().exec(function (err, docs) {
      res.render('AddNewDepartment', {
        employee_list: docs
      });
    });
  }
});

/*GET Edit Department Page*/
router.get("/EditDepartment", function (req, res) {
  // Check login first
  var backURL = req.header('Referer') || '/';
  if (!req.session.email) {
    res.send('You have not logged in, please log in first!<hr><a href="' + backURL + '">Go back the previous page</a>');
    return;
  } // -- end check login
  if (req.query.do_edit) { // Do Edit
    // Validate data
    if (validateDepartment(req.query.department_id,
            req.query.department_name,
            req.query.office_number,
            req.query.manager_id)) { // Success
      Department.update({'_id': req.query.department_id}, {
        "department_name": req.query.department_name.trim(),
        "office_number": req.query.office_number.trim(),
        "manager_id" : req.query.manager_id
      }, {multi: false}, function (err, result) {
        if (err) {
          req.session.message = 'Error while updating data:' + err;
        } else {
          req.session.message = 'Data updated';
          res.redirect('ViewDepartment?department_id=' + req.query.department_id);
        }
      });
    } else { // Failed
      req.session.message = 'Department data were not validated, please check again!';
      req.session.back_data = {// Send failed data back to register form
        "department_name": req.query.department_name,
        "office_number": req.query.office_number,
        "manager_id": req.query.manager_id
      };
      res.redirect(backURL);
    }
  } else { // Load edit form
    Employee.find().exec(function (err, docs) {
      Department.findOne({'_id': req.query.department_id}, function (err, doc) {
        res.render('EditDepartment', {
          department: doc,
          employee_list: docs
        });
      });
    });
  }
});

module.exports = router;
