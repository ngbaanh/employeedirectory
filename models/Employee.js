var express = require('express');
var router = express.Router();
/*MONGOOSE Shema*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Department = require('../models/Department');

var EmployeeSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  name: String,
  email: String,
  password: String,
  address: String,
  job_title: String,
  avatar_url: String,
  department: {type: Schema.Types.ObjectId, ref: 'Department'}
});

var Employee = mongoose.model('Employee', EmployeeSchema);
module.exports = Employee;