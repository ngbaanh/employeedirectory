var express = require('express');
var router = express.Router();
/*MONGOOSE Shema*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Employee = require('../models/Employee');

var DepartmentSchema = new Schema({
  _id: {type: Schema.Types.ObjectId},
  department_name: String,
  office_number: String,
  manager: {type: Schema.Types.ObjectId, ref: 'Employee'}
});

var Department = mongoose.model('Department', DepartmentSchema);
module.exports = Department;